/**
  Loading all dependencies.
**/
var express         =     require("express");
var redis           =     require("redis");

//注意node中mysql的用法
var mysql           =     require("mysql");

//注意express中session和redis的搭配使用
var session         =     require('express-session');
var redisStore      =     require('connect-redis')(session);
var bodyParser      =     require('body-parser');
var cookieParser    =     require('cookie-parser');
var path            =     require("path");
var async           =     require("async");
var client          =     redis.createClient();
var app             =     express();
var router          =     express.Router();

// Always use MySQL pooling.
// Helpful for multiple connections.
//使用mysql连接池
var pool    =   mysql.createPool({
    connectionLimit : 100,
    host     : 'localhost',
    port     : 8889,
    user     : 'root',
    password : 'root',
    database : 'redis_demo',
    debug    :  false
});

//注意__dirname指的是当前文件所在的路径，不是项目路径
app.set('views', path.join(__dirname,'../','views'));
app.engine('html', require('ejs').renderFile);

// IMPORTANT
// Here we tell Express to use Redis as session store.
// We pass Redis credentials and port information.
// And express does the rest ! 

app.use(session({
    secret: 'ssshhhhh',
    store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl :  260}),
    saveUninitialized: false,
    resave: false
}));
app.use(cookieParser("secretSign#143_!223"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// This is an important function.
// This function does the database handling task.
// We also use async here for control flow.
//Async is a utility module which provides straight-forward, powerful functions for working with asynchronous JavaScript.
function handle_database(req,type,callback) {
  //Within each function of the array, there’s a parameter call callback passed in, this is the function async.parallel checks for during in each function execution.
  //队列中的每一个方法，都要传一个默认的callback进去，callback的实现由async.js实现，callback中的第一个参数为err，通过true和false判断队列中函数执行是否出现错误，队列中每一个方法执行后都要主动调用callback来延续下一个方法的执行。
  //callback中的其余参数可以定义来传递给下一个队列中的方法
  //callback要作为最后一个参数传递给每一个队列中的方法

  //将对数据库的操作做一个队列处理，这样写比嵌套多个callback要好
   async.waterfall([
    //第一个方法获取数据库连接，
    function(callback) {
        pool.getConnection(function(err,connection){
          if(err) {
             // if there is error, stop right away.
             // This will stop the async code execution and goes to last function.
             //err = true，终止队列执行
            callback(true);
          } else {
            //err = null，继续下一个队列，并传入connection
            callback(null,connection);
          }
        });
    },

    //第二个方法准备好sql语句
    //connection取到队列中上一个方法获取的mysql连接
    function(connection,callback) {
      var SQLquery;
      switch(type) {
        case "login" :
        SQLquery = "SELECT * from user_login WHERE user_email='"+req.body.user_email+"' AND `user_password`='"+req.body.user_password+"'";
        break;

        case "checkEmail" :
        SQLquery = "SELECT * from user_login WHERE user_email='"+req.body.user_email+"'";
        break;

        case "register" :
        SQLquery = "INSERT into user_login(user_email,user_password,user_name) VALUES ('"+req.body.user_email+"','"+req.body.user_password+"','"+req.body.user_name+"')";
        break;

        case "addStatus" :
        SQLquery = "INSERT into user_status(user_id,user_status) VALUES ("+req.session.key["user_id"]+",'"+req.body.status+"')";
        break;

        case "getStatus" :
        SQLquery = "SELECT * FROM user_status WHERE user_id="+req.session.key["user_id"];
        break;

        default :
        break;
      }
      //准备好sql语句后调用下一个方法并传入语句
      //err = null
      callback(null,connection,SQLquery);
    },

    //第三个方法检索数据库
    //队列中最后一个方法中的callback既是最后的waterfall中的callback
    function(connection,SQLquery,callback) {
       connection.query(SQLquery,function(err,rows){
              //数据库操作完毕释放连接
              connection.release();

              if(!err) {
                  console.log(rows);
                  if(type === "login") {
                    callback(rows.length === 0 ? false : rows[0]);
                  } else if(type === "getStatus") {
                    callback(rows.length === 0 ? false : rows);
                  } else if(type === "checkEmail") {
                    callback(rows.length === 0 ? false : true);
                  } else {
                    callback(false);
                  }
              } else {
                   // if there is error, stop right away.
                  // This will stop the async code execution and goes to last function.
                  callback(true);
               }
          });
       }],

       //队列中的所有方法执行完毕后会回调这个方法
       function(result){
          // This function gets call after every async task finished.
          //数据库操作出错时给callback传null
          if(typeof(result) === "boolean" && result === true) {
            callback(null);
          } else {
            //数据库操作正常时给callback传result，注意这里的callback才是调用waterfall时传递的callback
            callback(result);
          }
        });
}

/**
    --- Router Code begins here.
**/

router.get('/',function(req,res){
    res.render('index.html');
});

router.post('/login',function(req,res){
    //将对数据库的操作进行封装
    handle_database(req, "login", function(response){
        if(response === null) {
            res.json({"error" : "true","message" : "Database error occured"});
        } else {
            if(!response) {
              res.json({
                 "error" : "true",
                 "message" : "Login failed ! Please register"
               });
            } else {
               console.log(response);
               req.session.key = response;
               res.json({"error" : false,"message" : "Login success."});
            }
        }
    });
});

router.get('/home',function(req,res){
    if(req.session.key) {
        res.render("home.html",{ email : req.session.key["user_name"]});
    } else {
        res.redirect("/");
    }
});

router.get("/fetchStatus",function(req,res){
  if(req.session.key) {
    handle_database(req,"getStatus",function(response){
      if(!response) {
        res.json({"error" : false, "message" : "There is no status to show."});
      } else {
        res.json({"error" : false, "message" : response});
      }
    });
  } else {
    res.json({"error" : true, "message" : "Please login first."});
  }
});

router.post("/addStatus",function(req,res){
    if(req.session.key) {
      handle_database(req,"addStatus",function(response){
        if(!response) {
          res.json({"error" : false, "message" : "Status is added."});
        } else {
          res.json({"error" : false, "message" : "Error while adding Status"});
        }
      });
    } else {
      res.json({"error" : true, "message" : "Please login first."});
    }
});

router.post("/register",function(req,res){
    handle_database(req,"checkEmail",function(response){
      //这样写有点问题，把数据库错误也当成邮箱已注册了
      if(response === null) {
        res.json({"error" : true, "message" : "This email is already present"});
      } else {
        handle_database(req,"register",function(response){
          if(response === null) {
            res.json({"error" : true , "message" : "Error while adding user."});
          } else {
            res.json({"error" : false, "message" : "Registered successfully."});
          }
        });
      }
    });
});

router.get('/logout',function(req,res){
    if(req.session.key) {
      req.session.destroy(function(){
        res.redirect('/');
      });
    } else {
        res.redirect('/');
    }
});

app.use('/',router);

app.listen(3000,function(){
    console.log("I am running at 3000");
});
